//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./Item.sol";
import "./Ownable.sol";
import "@openzeppelin/contracts/utils/Create2.sol";


/**
     * @title A contract to create products {createItem}, trigger payments {triggerPayment} as well as trigger delivery {triggerDelivery}
     * @author Viktor V. Lavrenenko
     * @notice Only product owners can create Items {createItem}
     * @dev All function are currently implemented without side effects
     * @custom:experimental This is an experimental contract
     */
contract ItemManager is Ownable {
    address public owner;
    mapping(uint256 => SItem) public items;
    uint256 private index;
    bytes32 private constant SALT = "Hiroshima";

    struct SItem {
        ItemManager.SupplyChainSteps step;
        string identifier;
        Item item;
    }

    /**
     * @dev Sets the {owner} of the contract
     */
    constructor() {
        owner = msg.sender;
    }

    enum SupplyChainSteps {
        Created,
        Paid,
        Delivered
    }
    event SupplyChainStep(uint256 _itemIndex, uint256 _step, address _address);

    
    /**
     * @notice Creates a new product and adds it to the supply chain
     * Only the owner of the contract can use this function to create products
     * @dev Passes parameters and uses the salted contract creation via CREATE2 opcode to create the new product via Item contract
     * @param _identifier The string identifier of a product, its 
     * @param _priceInWei The number of wei set as price of the product
     */
    function createItem(string memory _indentifier, uint256 _priceInWei)
        public
        onlyOwner
    {
        Item item = (new Item){salt: SALT}(this, _priceInWei, index);
        items[index].item = item;
        items[index].identifier = _indentifier;
        items[index].step = SupplyChainSteps.Created;
        emit SupplyChainStep(index, uint256(items[index].step), address(item));
        index++;
    }

     /**
     * @notice Triggers a payment
     * @dev Sens crypto to an address of a product. The contract {Item} transfers the money to triggerPayment by calling the function  {triggerPayment} and passing the index of the product
     * @param _index The index of a product
     */
    function triggerPayment(uint256 _index) public payable {
        Item item = items[_index].item;
        require(item.priceInWei() == msg.value, "Not fully paid");
        require(
            items[_index].step == SupplyChainSteps.Created,
            "The item is in the supply chain!"
        );
        items[index].step == SupplyChainSteps.Paid;
        emit SupplyChainStep(_index, uint256(items[index].step), address(item));
    }

     /**
     * @notice Triggers a delivery process
     * Only the owner of the contract can use this function to ship products
     * @dev Passes the index of a product
     * @param _index The index of the product that will be shipped to its purchaser
     */
    function triggerDelivery(uint256 _index) public onlyOwner {
        require(
            items[index].step == SupplyChainSteps.Paid,
            "The item is in the supply chain!"
        );
        items[index].step = SupplyChainSteps.Delivered;
        emit SupplyChainStep(
            _index,
            uint256(items[index].step),
            address(items[index].item)
        );
    }
}
