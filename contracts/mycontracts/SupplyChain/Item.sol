//SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "./ItemManager.sol";

    /**
     * @title A contract to store product data
     * @author Viktor V. Lavrenenko
     * @dev All function are currently implemented without side effects
     * @custom:experimental This is an experimental contract
     */
contract Item{
    ItemManager private parentContract;
    uint public priceInWei;
    uint public paidWei;
    uint public index;
    address public owner;

    /**
     * @dev Sets the values for {parentContract}, {priceInWei} and {index}
     * All of these values are immutable, they can only be set during construction
     * @param _parentContract The address of the parent contract that manages the Supply Chain process
     * @param _priceInWei The price of the product in Wei
     * @param _index The index of the product
     */
    constructor(ItemManager _parentContract, uint _priceInWei, uint _index){

        parentContract = _parentContract;
        priceInWei = _priceInWei;
        index = _index;
    }

     /**
     * @dev Transfers crypto to the address of the contract. Contract delegates the payment to the ItemManager contract by calling its {triggerPayment} function and sharing the index of the product with it.
     */
    receive () external payable{
        require(priceInWei == msg.value, "Nor support for partial payments");
        require(paidWei == 0, "It's already paid");
        paidWei += msg.value;
        (bool success, ) = address(parentContract).call{value:msg.value}(abi.encodeWithSignature("triggerPayment(uint256)", index)); 
        require(success, "Payment failed");
    }
}