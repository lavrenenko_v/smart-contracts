// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;
import "./FakeContract.sol";

contract Deployer{

 address public precomputedAddress;   
 address public deployedAddress;
 
  function deploy(bytes32 salt, bytes memory contractBytecode) public {
    bytes memory bytecode = contractBytecode;
    address addr;
      
    assembly {
      addr := create2(0, add(bytecode, 0x20), mload(bytecode), salt)
    }
    deployedAddress = addr;
  }


  function computeAddress(
        bytes32 salt,
        bytes32 bytecodeHash,
        address deployer
    ) external{
        bytes32 _data = keccak256(abi.encodePacked(bytes1(0xff), deployer, salt, bytecodeHash));
        precomputedAddress = address(uint160(uint256(_data)));
    }
}