// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;
import "./FuzzyIdentityChallenge.sol";


interface IName {
    function name() external view returns (bytes32);
}

contract Fake is IName{
    function name() external override pure returns(bytes32){
        return bytes32("smarx");
    }

    function authenticate(address _addr) public{
        FuzzyIdentityChallenge target = FuzzyIdentityChallenge(_addr);
        target.authenticate();
    }
}



