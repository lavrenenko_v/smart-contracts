// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;
import "./FakeContract.sol";
contract FuzzyIdentityChallenge {
    bool public isComplete;


    function authenticate() public {
        require(isSmarx(msg.sender), "You are not the owner");
        require(isBadCode(msg.sender), "Unallowed address");
        
        isComplete = true;
    }

    function isSmarx(address addr) internal view returns (bool) {
        return IName(addr).name() == bytes32("smarx");
    }

    function isBadCode(address _addr) internal pure returns (bool) {
        bytes20 addr = bytes20(_addr);
        bytes20 id = hex"000000000000000000000000000000000badc0de";
        bytes20 mask = hex"000000000000000000000000000000000fffffff";

        for (uint256 i = 0; i < 34; i++) {
            if (addr & mask == id) {
                return true;
            }
            mask <<= 4;
            id <<= 4;
        }

        return false;
    }
}